<?php
namespace LocknLoad\Crud;

/**
 * Fields
 *
 * @uses BaseController
 * @package locknload\Crud
 * @version //autogen//
 * @copyright Copyright (c) 2010 All rights reserved.
 * @author Davi Menegotto
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 */
class Fields
{

    static private $typeText     = ['/text/','/smalltext/', '/mediumtext/','/longtext/'];
    static private $typeInt      = ['/int/'];
    static private $typeDecimal  = ['/decimal/'];
    static private $typeVarchar  = ['/varchar/'];
    static private $typeSelect   = ['/enum/','/id_/','/select/'];
    static private $typeDate     = ['/date/','/dt_/'];
    static private $typeImage    = ['/image/'];
    static private $typeColor    = ['/cor/', '/hexa/'];
    static private $typeBool     = ['/^b_/'];
    static private $typeRelation = ['/^id_/'];
    static private $specialIdentifyers = array("_token","id_linha","acessos","id","tamanho","b_","multi_","_wysihtml5_mode","dt_","image_","data_");

    public static function isRelation($column){

        foreach (self::$typeRelation as $item) {
            if( preg_match($item, $column['field']) && !preg_match('/'.env('CRUD_IMG_FIELD',"id_gb_image").'/', $column['field']) ) return true;
        }

        return false;

    }

    public static function isDecimal($column){

        foreach (self::$typeDecimal as $item) {
            if( preg_match($item, $column['type']) ) return true;
        }

        return false;

    }

    public static function isVarchar($column){

        foreach (self::$typeVarchar as $item) {
            if( preg_match($item, $column['type']) ) return true;
        }

        return false;

    }

    public static function isInt($column){

        foreach (self::$typeInt as $item) {
            if( preg_match($item, $column['type']) ) return true;
        }

        return false;

    }

    public static function isText($column){

        foreach (self::$typeText as $item) {
            if( preg_match($item, $column['type']) ) return true;
        }

        return false;

    }

    public static function isSelect($column){

        foreach (self::$typeSelect as $item) {
            if( preg_match($item, $column['type']) ) return true;
            else if( preg_match($item, $column['field']) && !preg_match('/'.env('CRUD_IMG_FIELD',"id_gb_image").'/', $column['field']) ) return true;
        }

        return false;

    }

    public static function isDate($column){

        foreach (self::$typeDate as $item) {
            if( preg_match($item, $column['type']) ) return true;
        }

        return false;

    }

    public static function isImage($column){

        foreach (self::$typeImage as $item) {
            if( preg_match($item, $column['field'] )) return true;
        }

        return false;

    }

    public static function isColor($column){

        foreach (self::$typeColor as $item) {
            if( $item == $column['field'] ) return true;
        }

        return false;

    }

    public static function isBool($column){

        foreach (self::$typeBool as $item) {
            if( preg_match($item, $column['field']) ) return true;
        }

        return false;

    }


    static function setBooleans($obj, $columns, $inputs) {

        foreach ($columns as $key => $column) {

            $name = $column['field'];

            if ( preg_match("/b_/i", $name) && !preg_match("/".env('CRUD_IMG_FIELD',"id_gb_image")."/",$name) ) {
                if (isset($inputs[$name])) $obj->$name = 1;
                else $obj->$name = 0;
            }

        }

    }

    static function setExtraValues($obj, $inputs) {

        foreach ($inputs as $key => $value) {
            if (!preg_match("/".env('CRUD_IMG_FIELD',"id_gb_image")."/", $key) && Self::isNormalField($key, $obj)) {
                $obj->$key = !empty($value)? $value : null;
            }
        }

    }

    static function setDates($obj, $inputs) {
        foreach ($inputs as $key => $value) {
		if ( preg_match("/dt_/i", $key) ||  preg_match("/data_/i", $key)  ) {
                 $date = \DateTime::createFromFormat('d/m/Y', $value);
                 if ($date){
                     $usableDate = $date->format('Y-m-d H:i:s');
                     $obj->$key  = $usableDate;
                 }
            }
        }
    }

    static function setRelations($obj, $inputs){

        foreach ($inputs as $key => $value){

            if( preg_match("/id_/",$key) && !preg_match("/".env('CRUD_IMG_FIELD',"id_gb_image")."/",$key) ){
                if ($value == 0) $obj->$key = null;
                $obj->$key = $value;
            }

        }

    }

    static function setManytoMany($obj, $inputs){

        foreach ($inputs as $key => $value){

            if( preg_match("/multi_/",$key) ){
                $item   = explode("_", $key);
                $method = $item[sizeof($item)-1];
                if(is_array($value)) $obj->$method()->syncWithoutDetaching($value);
                else $obj->$method()->syncWithoutDetaching([$value]);
            }

        }

    }

    static function setImages($obj, $inputs) {

      if (sizeof($_FILES) > 0) {
        foreach($_FILES as $item => $file) {
          foreach($inputs as $key => $i){
            if (!empty($file['tmp_name']) &&  $file['size'] > 0 && $item == $key) {
              $obj->$key = self::saveImage($file, env('CRUD_IMG_FIELD',"id_gb_image"));
            }
          }
        }
      } else if (sizeof($_FILES) == 0) {
        //  if ($value == 0) $obj->$key  = null;
        //  else $obj->$key  = $value;
      }

    }
    /*
     *
     * The project must have a MODEL for image upload.
     * This model must have a method imgoutput, that returns a path to save the images
     * After the upload, it will optimize the images using optipng and jpegoptim
     *
     * */
    static function saveImage($file, $field){

        $imgClass = Helper::generateModel(env('CRUD_IMG_TABLE',"gb_image"));
        $gb_imagem = new $imgClass();
        $gb_imagem->imagem = 'storage/'.$file['name'];
        $gb_imagem->tamanho = $file['size'];
        $gb_imagem->save();

        file_put_contents(storage_path().'/app/public/'.$file['name'], file_get_contents($file['tmp_name']));

        if ( pathinfo($gb_imagem->imagem, PATHINFO_EXTENSION) == "png" ) {
            exec('optipng '.storage_path().'/app/public/'.$file['name']);
        } else if ( pathinfo($gb_imagem->imagem, PATHINFO_EXTENSION) == "jpg" ) {
            exec('jpegoptim -o '.storage_path().'/app/public/'.$file['name']);
        } file_put_contents(storage_path().'/app/public/'.$file['name'], file_get_contents($file['tmp_name']));

        return $gb_imagem->imagem;

    }

    static function isNormalField($key, $obj = array()){

        foreach(self::$specialIdentifyers as $item){
            if ( preg_match("/".$item."/i", $key) ) return false;
        }

        foreach( $obj::getManyToMany() as $item){
            if ( preg_match("/".$item."/i", $key) ) return false;
        }

        if ( preg_match("/multi_/i", $key) ) return false;

        return true;
    }

    public static function defineFieldType($field){

            if ( self::isBool($field) )           return "bool";
            else if ( self::isSelect($field) )    return "select";
            else if ( self::isImage($field) )     return "image";
            else if ( self::isRelation($field) )  return "relations";
            else if ( self::isInt($field) )       return "int";
            else if ( self::isText($field) )      return "text";
            else if ( self::isVarchar($field) )   return "varchar";
            else if ( self::isDecimal($field) )   return "decimal";
            else if ( self::isDate($field) )      return "date";

    }

    public static function setFieldInfo($field){

        $info     = array();
        $dataType = explode("(",$field->Type);
        $type     = self::defineFieldType(
                        array('field' => $field->Field, 'type' => $dataType[0])
                    );

        if( isset($dataType[1]) ) $op = explode(")",$dataType[1])[0];

        $info['field']     = $field->Field;
        $info['nullable']  = $field->Null;
        $info['default']   = $field->Default;
        $info['type']      = $type;
        $info['form_size'] = $type == 'image' || $type == 'text'? 'big':'small';

        if ($info['type'] == 'select' && preg_match("/id_/",$info['field'])) $info['relation'] = true;

        if( isset($op) ) {
            $extra =  explode(",", str_replace("'","",$op) );

            if (sizeof($extra) > 1) $info['enum'] = $extra;
            else $info['size'] = $extra[0];
        }

        return $info;

    }
}
