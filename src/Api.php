<?php
namespace LocknLoad\Crud;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use LocknLoad\Crud\Helper as Helper;

/**
 * Api
 *
 * @uses BaseController
 * @package locknload\Crud
 * @version //autogen//
 * @copyright Copyright (c) 2010 All rights reserved.
 * @author Davi Menegotto
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 */
class Api extends BaseController
{

  public function liveEditorLine($id = null){

  }

  public function liveEditorModel($id = null){

  }

  public function addImage(Request $request, $relationMTM = null, $id = null) {

      $model      = Helper::generateModel(env('CRUD_IMG_TABLE','gb_image'));
      $columns    = Helper::getModelColumns( new $model() );
      $relation   = ($relationMTM)? Helper::generateModel( $relationMTM ) : null;
      $inputs     = $request->all();
      $desktop    = null;

      $gb_imagem = new $model;
      $relation  = ($id) ? $relation::find($id) : null;

      if (sizeof($_FILES) > 0){
        foreach($_FILES as $key => $i) {
            if (!empty($i['tmp_name']) && $i['size'] > 0) {
                $file = $i;
            }
        }

        $gb_imagem->imagem = $gb_imagem->imgoutput().$file['name'];

        if (!empty($file['tmp_name'])) {
            file_put_contents(public_path().$gb_imagem->imagem, file_get_contents($file['tmp_name']));

            if ( pathinfo($gb_imagem->imagem, PATHINFO_EXTENSION) == "png" ) {
              exec('optipng '.public_path().$gb_imagem->imagem);
            } else if ( pathinfo($gb_imagem->imagem, PATHINFO_EXTENSION) == "jpg" ) {
              exec('jpegoptim -o '.public_path().$gb_imagem->imagem);
            }

            $gb_imagem->save();

            if ($relation) $relation->image()->attach([$gb_imagem->id]);
        }
      }
  }

}
