<?php
namespace LocknLoad\Crud;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use LocknLoad\Crud\Helper as Helper;

/**
 * Crud
 *
 * @uses BaseController
 * @package locknload\Crud
 * @version //autogen//
 * @copyright Copyright (c) 2010 All rights reserved.
 * @author Davi Menegotto
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 */
class Crud extends BaseController
{

    private $IMAGE_TABLE = null;

    public function __construct(){
        $this->IMAGE_TABLE = env('CRUD_IMG_TABLE','gb_image');
    }

    /* public listar($class, $filtro = null, $condicao = null) {{{ */
    /**
     * listar
     *
     * @param mixed $class
     * @param bool $filtro
     * @param bool $condicao
     * @access public
     * @return void
     */
    public function list($class, $filter = null, $condition = null, $filtroEncadeado = null)
    {
        $objs     = null;
        $model    = Helper::generateModel($class);
	    $columns  = Helper::getModelColumns( new $model(), true );
        $orderBy  = $model::getOrderBy('id','desc');

        $logger   = Helper::generateModel('usr_log');
        $logger::generate('listar',Helper::generateModel($class));

        if($filter) {

            if ( in_array($filter, $model::getManyToMany() ) ) {

                $filter = explode("_",$filter);
                array_shift($filter);

                if ( sizeof($filter) > 1 ) $filter = implode("_",$filter);
                else $filter = $filter[0];

                $objs   = $model::whereHas($filter, function($query) use ($condition, $filtroEncadeado) {
                    $query->where('id',$condition);
                })->paginate(20);

            } else {

                if ($orderBy) {
                    if ($condition) {
                      if($condition == "exist"){
                        $objs = $model::whereNotNull($filter)
                                  ->whereNull("deleted_at")
                                  ->orderBy($orderBy,'asc')
                                  ->orderBy('created_at','desc')
                                  ->paginate(20);
                      } else if($condition == "notexist"){
                        $objs = $model::whereNull($filter)
                                  ->whereNull("deleted_at")
                                  ->orderBy($orderBy,'asc')
                                  ->orderBy('created_at','desc')
                                  ->paginate(20);
                      } else if($condition == 'has'){
                        $model::has($filter)
                                  ->whereNull("deleted_at")
                                  ->orderBy($orderBy,'asc')
                                  ->orderBy('created_at','desc')
                                  ->paginate(20);
                      } else {
                        $objs = $model::where($filter,$condition)
                              ->whereNull("deleted_at")
                              ->orderBy($orderBy,'asc')
                              ->orderBy('created_at','desc')
                              ->paginate(20);
                      }
                    } else {
                        $objs = $model::where($filter,1)
                            ->whereNull("deleted_at")
                            ->orderBy($orderBy,'asc')
                            ->orderBy('created_at','desc')
                            ->paginate(20);
                    }
                } else {
                    if ($condition) {
                        if($condition == "exist") {
                            $objs = $model::whereNotNull($filter)
                                      ->whereNull("deleted_at")
                                      ->orderBy('created_at','desc')
                                      ->paginate(20);
                        } else if($condition == "notexist") {
                            $objs = $model::whereNull($filter)
                                      ->whereNull("deleted_at")
                                      ->orderBy('created_at','desc')
                                      ->paginate(20);
                        } else if($condition == 'has'){
                          $model::has($filter)
                                ->whereNull("deleted_at")
                                ->orderBy('created_at','desc')
                                ->paginate(20);
                        } else {
                            $objs = $model::where($filter,$condition)
                                      ->whereNull("deleted_at")
                                      ->orderBy('created_at','desc')
                                      ->paginate(20);
                        }
                    } else {
                        $objs = $model::where($filter,1)
                            ->whereNull("deleted_at")
                            ->orderBy('created_at','desc')
                            ->paginate(20);
                    }
                }
            }

        } else {

            if ($orderBy) {
                $q =  $model::whereNull("deleted_at");

                if($model::getExcludeArr()){
                  $q->where($model::getExcludeArr()['key'], "<", 9);
                }

                $q->orderBy('id','desc')
                  ->orderBy($orderBy,'asc');

                 $objs = $q->paginate(20);
            } else {
                 $objs = $model::orderBy('id','desc')
                    ->whereNull("deleted_at")
                    ->paginate(20);
            }

        }

        if($filtroEncadeado){
          $q = $model::whereNull("deleted_at");

          foreach($filtroEncadeado as $key => $f){
            if(!preg_match("/page/",$key)) {
               if (preg_match("/id_/",$key)){
                  $filter = explode("_",$key);
                  $filter = $filter[sizeof($filter)-1];

                  $q->with([$filter => function($q) use ($f) {
                      $q->where('id',$f);
                  }]);
                }

                $q->where($key,'like','%'.$f.'%');
            }
          }

          $objs = $q->paginate(20);
	    }

        return array('objs' => $objs, 'columns' => $columns, 'class' => $class);

    }
    /* }}} */

    public function listCrud(Request $request, $class, $filter = null, $condition = null) {

        $inputs       = array_filter($request->all());

        $result       = $this->list($class, $filter, $condition, $inputs);
        $result['qs'] = $inputs;

        return Helper::generateView('crud.list', $result);

    }

    public function listApi($class, $filter = null, $condition = null) {

        $result = $this->list($class, $filter, $condition);

        return Helper::generateJson($result['objs']);

    }

    public function edit($class, $id = null)
    {
        $model      = Helper::generateModel($class);
        $obj        = new $model();
        $columns    = Helper::getModelColumns( $obj );
        $struct     = $obj->getStruct();
        $multipart  = true;
        $liveeditor = false;
        $filters    = false;
        $combination= false;
        $dropzone   = false;

        $logger   = Helper::generateModel('usr_log');
        $logger::generate('editar',Helper::generateModel($class));

        foreach ($columns as $col) {
            if ( $col['field'] == 'id_'.$this->IMAGE_TABLE ) {
                $multipart = true;
            }
        }

        $columns = $this->sortByStruct($obj, $columns, $struct);

        if ($id) {
            $filters    = (method_exists($obj, 'getFilters'))   ? Helper::getFilterItems($obj->getFilters())  : null;
            $combination= ($obj->getCombinationTable())  ? true : false;
            $liveeditor = (method_exists($obj, 'useTemplate'))  ? true : false;
            $dropzone   = ($obj->getDropzone())  ? true : false;
            $obj        = $model::find($id);
        }

        return Helper::generateView('crud.persist', array(
            'obj'        => $obj,
            'class'      => $class,
            'model'      => $model,
            'struct'     => $struct,
            'filters'    => $filters,
            'combination'=>$combination,
            'columns'    => $columns,
            'multipart'  => $multipart,
            'liveeditor' => $liveeditor,
            'dropzone'   => $dropzone
        ));
    }

    public function editApi($class, $id = null)
    {
        $model      = Helper::generateModel($class);
        $obj        = new $model();
        $columns    = Helper::getModelColumns( $obj );
        $struct     = $obj::getStruct();
        $multipart  = true;

        foreach ($columns as $col) {
            if ( $col['field'] == 'id_'.$this->IMAGE_TABLE ) {
                $multipart = true;
            }
        }

        $columns = $this->sortByStruct($obj, $columns, $struct);

        if ($id) {
            $obj        = $model::find($id);
        }

        return Helper::generateView('crud.formAjax', array(
            'obj'        => $obj,
            'class'      => $class,
            'model'      => $model,
            'struct'     => $struct,
            'columns'    => $columns,
            'multipart'  => $multipart,
        ));
    }

    public function delete(Request $request, $class)
    {
        $inputs     = $request->all();
        $id         = $inputs['id'];

        $logger   = Helper::generateModel('usr_log');
        $logger::generate('deletar',Helper::generateModel($class));

        if ($id != 0) {
            $model = Helper::generateModel($class);
            $model::destroy($id);

            return response()->json(['status' => 'success', 'msg' => 'Item is succesfull deleted']);
        }

        return response()->json(['status' => 'fail', 'msg' => 'There is no item to delete']);

    }

    public function saveImage(Request $request) {

        $model      = Helper::generateModel(env('CRUD_IMG_TABLE','gb_image'));
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();
        $id         = $inputs['id'];
        $desktop    = null;
        $mobile     = null;

        if ($id == 0) $gb_imagem = new $model;
        else $gb_imagem = $model::find($inputs['id']);

        if (sizeof($inputs) > 0 && sizeof($_FILES) > 0){

            foreach($_FILES as $key => $file) {
                if (!empty($file['tmp_name']) && $file['size'] > 0) {
                    if ($key == 0) $desktop = $file;
                    else $mobile = $file;
                }
            }

            if ($mobile) $gb_imagem->mobile = $gb_imagem->imgoutput().$mobile['name'];

            $gb_imagem->imagem      = $gb_imagem->imgoutput().$desktop['name'];
            $gb_imagem->nome        = isset($input['nome'])?$input['nome']:"";
            $gb_imagem->title       = isset($input['title'])?$input['title']:"";
            $gb_imagem->credito     = isset($input['credito'])?$input['credito']:"";
            $gb_imagem->alt         = isset($input['alt'])?$input['alt']:"";
            $gb_imagem->descricao   = isset($input['descricao'])?$input['descricao']:"";
            $gb_imagem->tamanho     = isset($file['size'])?$file['size']:"";
            $gb_imagem->save();

            if (!empty($file['tmp_name'])) {

                file_put_contents(public_path().$gb_imagem->imagem, file_get_contents($file['tmp_name']));
                if ($mobile) file_put_contents(public_path().$gb_imagem->mobile, file_get_contents($mobile['tmp_name']));

                if ( pathinfo($gb_imagem->imagem, PATHINFO_EXTENSION) == "png" ) {
                    exec('optipng '.public_path().$gb_imagem->imagem);
                } else if ( pathinfo($gb_imagem->imagem, PATHINFO_EXTENSION) == "jpg" ) {
                    exec('jpegoptim -o '.public_path().$gb_imagem->imagem);
                }

                if ($mobile) {
                    if ( pathinfo($gb_imagem->mobile, PATHINFO_EXTENSION) == "png" ) {
                        exec('optipng '.public_path().$gb_imagem->mobile);
                    } else if ( pathinfo($gb_imagem->mobile, PATHINFO_EXTENSION) == "jpg" ) {
                        exec('jpegoptim -o '.public_path().$gb_imagem->mobile);
                    }
                }

            }

            if(preg_match('/gb_image/',Session::get('_previous')['url']) ){
            	return redirect('/listar/gb_image');
            } else {
                return response()->json(['status' => 'success', 'msg' => 'Item has been inserted on DB']);
            }

        }
        if(preg_match('/gb_image/',Session::get('_previous')['url']) ){
            return redirect('/listar/gb_image');
        } else {
            return response()->json(['status' => 'fail', 'msg' => 'There is no data to store']);
        }

    }

    public function save(Request $request, $class) {

        $model      = Helper::generateModel($class);
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();
        $id         = $inputs['id'];

        if (sizeof($inputs) > 0){

            if ($id == 0) $obj = new $model;
            else $obj = $model::find($inputs['id']);

            Fields::setBooleans($obj, $columns, $inputs);
            Fields::setDates($obj, $inputs);
            Fields::setExtraValues($obj, $inputs);
            Fields::setRelations($obj, $inputs);
            Fields::setImages($obj, $inputs);

	        $obj->save();

            Fields::setManyToMany($obj, $inputs);

            $request->session()->flash('status', 'Sucesso | Dados inseridos na base.' );
            return Redirect::to('/listar/'.$class);

        }

        $request->session()->flash('status', 'Error | Verifique os dados inseridos.');
        return Redirect::back();

    }

    public function saveApi(Request $request, $class) {

        $model      = Helper::generateModel($class);
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();
        $id         = $inputs['id'];

        if (sizeof($inputs) > 0){

            if ($id == 0) $obj = new $model;
            else $obj = $model::find($inputs['id']);

            Fields::setBooleans($obj, $columns, $inputs);
            Fields::setImages($obj, $inputs);
            Fields::setDates($obj, $inputs);
            Fields::setExtraValues($obj, $inputs);
            Fields::setRelations($obj, $inputs);

            $obj->save();

            Fields::setManyToMany($obj, $inputs);

            return response()->json(['status' => 'success', 'msg' => 'Item has been inserted on DB']);

        }

        return response()->json(['status' => 'fail', 'msg' => 'There is no data to store']);

    }

     public function saveMtmApi(Request $request, $class) {

        $model      = Helper::generateModel($class);
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();
        $id         = $inputs['id'];

        if (sizeof($inputs) > 0){

            $relation = $inputs['relation'];

            $obj = $model::find($inputs['id']);

            $obj->$relation()->attach([
                $inputs['relationValue'] => [$inputs['extraField'] => $inputs['extraFieldValue']]
            ]);

            return response()->json(['status' => 'success', 'msg' => 'Item has been inserted on DB']);

        }

        return response()->json(['status' => 'fail', 'msg' => 'There is no data to store']);

    }

    public function changeMtmApi(Request $request, $class) {

        $model      = Helper::generateModel($class);
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();
        $id         = $inputs['id'];

        if (sizeof($inputs) > 0){

            $relation = $inputs['relation'];
            $obj = $model::find($inputs['id']);
            $obj->$relation()->updateExistingPivot($inputs['relationValue'], [$inputs['extraField'] =>  $inputs['extraFieldValue']]);

            return response()->json(['status' => 'success', 'msg' => 'Item has been inserted on DB']);

        }

        return response()->json(['status' => 'fail', 'msg' => 'There is no data to store']);

    }

    public function deleteMtmApi(Request $request, $class) {

        $model      = Helper::generateModel($class);
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();
        $id         = $inputs['id'];

        if (sizeof($inputs) > 0){

        $relation = $inputs['relation'];
        $obj = $model::find($inputs['id']);
        $filtro = null;

        foreach($obj::getManyToMany() as $mtm){
            if(preg_match('/'.$relation.'/', $mtm)) {
                $filtro = $mtm;
                break;
            }
        }


        DB::table($class."s_has_".$filtro."s")
            ->where($class.'s_id', '=', $inputs['id'])
            ->where($filtro.'s_id', '=', $inputs['relationValue'])
            ->where($inputs['extraField'], '=',  $inputs['extraFieldValue'])->delete();

            return response()->json(['status' => 'success', 'msg' => 'Item has been inserted on DB']);

        }

        return response()->json(['status' => 'fail', 'msg' => 'There is no data to store']);

    }

     public function saveCombinationApi(Request $request, $class) {

        $model      = Helper::generateModel($class);
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();

        if (sizeof($inputs) > 0){

            $obj = $model::find($inputs['ec_produto_id']);
            $obj->combination()->attach($inputs['ec_disciplina_id'], [
                'ec_professor_id' => $inputs['ec_professor_id'],
                'valor' => $inputs['valor'],
                'carga_horaria' => $inputs['carga_horaria'],
            ]);

            return response()->json(['status' => 'success', 'msg' => 'Item has been inserted on DB']);

        }

        return response()->json(['status' => 'fail', 'msg' => 'There is no data to store']);

    }

    public function changeCombinationApi(Request $request, $class) {

        $model      = Helper::generateModel($class);
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();

        if (sizeof($inputs) > 0){

            $obj = $model::find($inputs['ec_produto_id']);
            $obj->combination()->updateExistingPivot(
                 $inputs['ec_disciplina_id'], [
                'ec_professor_id' => $inputs['ec_professor_id'],
                'valor' => $inputs['valor'],
                'carga_horaria' => $inputs['carga_horaria'],
            ]);

            return response()->json(['status' => 'success', 'msg' => 'Item has been inserted on DB']);

        }

        return response()->json(['status' => 'fail', 'msg' => 'There is no data to store']);

    }

    public function deleteCombinationApi(Request $request, $class) {

        $model      = Helper::generateModel($class);
        $columns    = Helper::getModelColumns( new $model() );
        $inputs     = $request->all();

        if (sizeof($inputs) > 0){

           $obj = $model::find($inputs['ec_produto_id']);
           $tb = "ec_produtos_has_ec_disciplinas";

            return response()->json(['status' => 'success', 'msg' => 'Item has been inserted on DB']);

        }

        return response()->json(['status' => 'fail', 'msg' => 'There is no data to store']);

    }

    public function sortByStruct($model, $data, $struct){

        foreach ($struct as $sectionk => $section){
            foreach($section as $itemk => $item){
                if (is_array($item) && array_key_exists('field',$item)) {
                    $fieldInfo = Helper::getColumnInfo($model, $item['field']);

                    if ($fieldInfo) {
                        foreach ( $fieldInfo as $infok => $info ) {
                            if (!array_key_exists($infok,$item)) $struct[$sectionk][$itemk][$infok] = $info;
                        }
                    }
                }
            }
        }

        return $struct;
    }

    public function sortableFields($data) {

        $bools      = [];
        $int        = [];
        $text       = [];
        $enum       = [];
        $image      = [];
        $normal     = [];
        $decimal    = [];
        $relations  = [];

        foreach ($data as $item) {

            if ( Fields::isBool($item) )           $bools[]      = $item;
            else if ( Fields::isSelect($item) )    $enum[]       = $item;
            else if ( Fields::isImage($item) )     $image[]      = $item;
            else if ( Fields::isDecimal($item) )   $decimal[]    = $item;
            else if ( Fields::isInt($item) )       $int[]        = $item;
            else if ( Fields::isText($item) )      $text[]       = $item;
            else if ( Fields::isVarchar($item) )   $normal[]     = $item;

        }

        return array_merge($bools,$enum,$int,$decimal,$normal,$image,$text);

    }

   public function MediaGalery() {

        $imagens = \App\GbImage::orderBy('created_at','desc')->paginate(30);

        return Helper::generateView('crud.bancoImagens', array( 'imagens' => $imagens ));

    }

}
