<?php

Route::group(['middleware' => ['web','auth','admin']], function () {

    Route::get('banco-imagens',                         'LocknLoad\Crud\Crud@MediaGalery');
    Route::get('mediagalery',                           'LocknLoad\Crud\Crud@MediaGalery');
    Route::get('editar/{model}/{id?}',                  'LocknLoad\Crud\Crud@edit');
    Route::get('listar/{model}/{filtro?}/{condicao?}/{extra?}',  'LocknLoad\Crud\Crud@listCrud');
    Route::post('gb_image',                             'LocknLoad\Crud\Crud@saveImage');
    Route::post('gb_image/{relation?}/{id?}',           'LocknLoad\Crud\Api@addImage');
    Route::post('{model}',                              'LocknLoad\Crud\Crud@save');
    Route::put('{model}',                               'LocknLoad\Crud\Crud@save');
    Route::delete('{model}',                            'LocknLoad\Crud\Crud@delete');

    // Routes for api return in json
    Route::get('api/editar/{model}/{id?}',                  'LocknLoad\Crud\Crud@editApi');
    Route::get('api/listar/{model}/{filtro?}/{condicao?}',  'LocknLoad\Crud\Crud@listApi');
    Route::post('api/{model}',                              'LocknLoad\Crud\Crud@saveApi');
    Route::put('api/{model}',                               'LocknLoad\Crud\Crud@saveApi');
    Route::post('api/mtm/{model}',                          'LocknLoad\Crud\Crud@saveMtmApi');
    Route::put('api/mtm/{model}',                           'LocknLoad\Crud\Crud@changeMtmApi');
    Route::delete('api/mtm/{model}',                        'LocknLoad\Crud\Crud@deleteMtmApi');

    Route::post('api/combination/{model}',                  'LocknLoad\Crud\Crud@saveCombinationApi');
    Route::put('api/combination/{model}',                   'LocknLoad\Crud\Crud@changeCombinationApi');
    Route::delete('api/combination/{model}',                'LocknLoad\Crud\Crud@deleteCombinationApi');

});
