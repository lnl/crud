<?php

namespace LocknLoad\Crud;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocknLoad\Crud\Helper;
use LocknLoad\Crud\Fields;
use DB;

class ModelCore extends Model {

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public static function getColumnField(){
	    return isset(static::$columnFiled)? static::$columnFiled : null;
  	}

    public static function getFieldsInList(){
        return isset(static::$fieldsInList) && !empty(static::$fieldsInList)? static::$fieldsInList : null;
    }

    public static function getExtraActions(){
        return isset(static::$extraActions) && !empty(static::$extraActions)? static::$extraActions : null;
    }

    public static function getSearchField(){
        return isset(static::$searchField) && !empty(static::$searchField)? static::$searchField : null;
    }

    public static function getTemplatePath(){
        return isset(static::$templatePath) && !empty(static::$templatePath)? static::$templatePath : null;
    }

    public static function getOrderBy(){
        return isset(static::$orderBy)? static::$orderBy : null;
    }

    public static function getExcludeArr(){
      return isset(static::$excludeArr)? static::$excludeArr : null;
    }

    public static function getConbination(){
        return isset(static::$combination)? static::$combination : array();
    }

    public static function getDropzone(){
        return isset(static::$dropzone)? static::$dropzone : null;
    }

    public static function getManyToMany(){
        return isset(static::$manyToMany)? static::$manyToMany : array();
    }

    public static function getFilters(){
        return isset(static::$filters) && !empty(static::$filters)? static::$filters : null;
    }

    public function presentation(){
        return "";
    }

    public static function getCombinationTable(){
        return isset(static::$combination) && !empty(static::$combination)? static::$combination : null;
    }

    public function getColumns(){
        return DB::select('SHOW COLUMNS FROM '.$this->table);
    }

    public function getStruct(){
        $result = null;

        if( isset($this->struct) && !empty($this->struct) ) {
            $struct = $this->struct;
            $columns = $this->getColumns();

            foreach( $struct as $i => $group){
                foreach($group as $key => $item) {
                    $result[$i][$key] = $item;
                    foreach( $columns as $column ){
                        if ($column->Field == $key){
                            $result[$i][$key] = array_merge(Fields::setFieldInfo($column),$item);
                        }
                    }
                }
            }


        } else {
            $result = array(
                'container1' => array(
                    'container_header'  => null,
                )
            );

            $columns = $this->sortableFields($this->getColumns());

            foreach( $columns as $item ) {
                if ( Helper::isColumnValid($item['field']) ) {
                    $result['container1'][$item['field']] = array('field' => $item['field']);
                }
            }
        }

        return $result;
    }

    public function sortableFields($data) {

        $bools      = [];
        $int        = [];
        $text       = [];
        $enum       = [];
        $image      = [];
        $normal     = [];
        $decimal    = [];
        $relations  = [];
        $dateTime   = [];

        foreach ($data as $item) {

            if ( is_object($item) ) {
                $item = array_change_key_case((array) $item,CASE_LOWER);
            }

            if ( $item['field'] != "id" ) {

                if ( Fields::isBool($item) )           $bools[]      = $item;
                else if ( Fields::isSelect($item) )    $enum[]       = $item;
                else if ( Fields::isImage($item) )     $image[]      = $item;
                else if ( Fields::isDecimal($item) )   $decimal[]    = $item;
                else if ( Fields::isInt($item) )       $int[]        = $item;
                else if ( Fields::isText($item) )      $text[]       = $item;
                else if ( Fields::isVarchar($item) )   $normal[]     = $item;
                else if ( Fields::isDate($item) )      $dateTime[]   = $item;

            }
        }

        return array_merge($bools,$enum,$int,$decimal,$normal,$image,$text,$dateTime);

    }

    public function salvar($values){

        foreach ($values as $key => $value) {
            $this->$key = $value;
        }

        $this->save();

    }
}
