<?php

namespace LocknLoad\Crud;

use Closure;

class Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

	if (\Auth::user() &&  \Auth::user()->b_admin) {
            return $next($request);
     	}

	return redirect('/');
    }
}
