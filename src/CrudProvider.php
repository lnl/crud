<?php

namespace LocknLoad\Crud;

use Illuminate\Support\ServiceProvider;

class CrudProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router) 
    {
        include __DIR__. '/Crud.php';
        include __DIR__. '/Api.php';
        include __DIR__. '/Helper.php';
        include __DIR__. '/Fields.php';
        include __DIR__. '/ModelCore.php';
	include __DIR__. '/Middleware.php';
	

        if (! $this->app->routesAreCached()) {
            require __DIR__.'/Routes.php';
        }


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->singleton('Helper', function () {
            return new Helper();
       });

       $this->app->singleton('Fields', function () {
           return new Fields();
        });
    }
}
