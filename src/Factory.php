<?php
namespace LocknLoad\Crud;

class Factory
{

    /* public generateLabel(string $value, string $required) {{{ */
    /**
     * generateLabel return a string that represents the label of the field whit * if its not null.
     *
     * @ToDo change both parameter for an field array. Must go to an upper class
     *
     * @param string $value
     * @param string $required
     * @static
     * @access public
     * @return void
     */
    public static function generateLabel(string $value, string $required){
        $result = explode("_",$value);

        if ( sizeof($result) > 1) $value = $result[sizeof($result) - 1];
        if ( $required == "NO" ) $value .= " *";

        return $value;
    }
    /* }}} */

    /* public showListText($row, $col) {{{ */
    /**
     * showListText
     *
     * @description show text on list for each field
     *
     * @param mixed $row
     * @param mixed $col
     * @static
     * @access public
     * @return void
     */
    public static function showListText($row, $col){
        return $row[$col['field']];
    }
    /* }}} */

    /* public showListRelation($row, $col) {{{ */
    /**
     * showListRelation
     *
     * @description return the information that must be show on the list if this is a relation field
     *
     * @ToDo review this logic
     *
     * @param mixed $row
     * @param mixed $col
     * @static
     * @access public
     * @return void
     */
    public static function showListRelation($row, $col){
        $model      = Helper::generateModel($col['field']);
        $item       = $model::find($row[$col['field']]);

        return $item ? $item->presentation() : null;
    }
    /* }}} */

    public static function getTextArea($row, $data){

        $content = "<label>".Macros::generateLabel($row['campo'], $row['null'])."</label>";
        $content .= "<textarea name=\"".$row['campo'] ."\" rows=\"20\">".$data['obj'][$row['campo']]."</textarea>";

        return $content;

    }

    public static function getSelected($row, $data){
        $content  = "<select name=\"".$row['campo']."\" class=\"form-control\">";

        if ( isset($data['obj'][$row['campo']]) ) $content .= "<option value=\"".$data['obj'][$row['campo']]."\" selected>".$data['obj'][$row['campo']]."</option>";

        foreach($row['opcoes'] as $op) $content .= "<option value=\"".$op."\">".$op."</option>";

        $content .= "</select>";

        return $content;
    }

    public static function getText($row, $data){
        return "<input type=\"text\" class=\"form-control\" placeholder=\"".$row['campo']."\" name=\"".$row['campo']."\" value=\"".$data['obj'][$row['campo']]."\">";
    }

    public static function getCheckBox($row, $data){

        if ( !empty($data['obj'][$row['campo']]) ) return "<input type=\"checkbox\" name=\"".$row['campo']."\" checked />";
        else return "<input type=\"checkbox\" name=\"". $row['campo'] ."\" />";

    }

    public static function getRelation($row, $data){

        $relacao     = explode("id_",$row['campo'])[1];
        $model       = env('APP_NAMESPACE').'\\'.studly_case($relacao);
        $collections = $model::get();
        $result      = "<select name=\"".$row['campo']."\" class=\"form-control\">";

        if( empty( $data['obj'][$row['campo']] ) ) $result .= "<option value=\"0\">Selecione</option>";

        foreach ($collections as $key => $value) {

            if( !empty($value['nome']) || !empty($value['categoria']) || !empty($value['email']) ) {

                if( $data['obj'][$row['campo']] == $value['id'] ) $result .= "<option value=\"".$value['id']."\" selected>";
                else $result .= "<option value=\"".$value['id']."\">";

                if( !empty($value['nome']) )            $result .= $value['nome'];
                else if( !empty($value['categoria']) )  $result .= $value['categoria'];
                else if( !empty($value['email']) )      $result .= $value['email'];

                $result .= "</option>";

            }

        }

        return $result .= "</select>";
    }

    public static function getFile($row){
        return "<input class=\"form-control\" type=\"file\" value=\"\" name=\"". $row['campo'] ."\"  placeholder=\"". $row['campo'] ."\" />";
    }

}
