<?php
namespace LocknLoad\Crud;

use LocknLoad\Crud\Helper;

/**
 * Service
 *
 * @package LocknLoad.Crud
 * @version //autogen//
 * @copyright LocknLoad technology (http://www.lockload.me) 2016 All rights reserved.
 * @author Davi Menegotto (supmouse)
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 *
 * @description This class must be resposable for methods to generate services
 * for Factory classes in both packeges Crud and Admin
 */
class Service
{

    /* public generateExtra(array $extra, array $field = array()) {{{ */
    /**
     * generateExtra
     *
     * @description generate extra properties for tags html
     *
     * @ToDo verify code to inplement spaces on array using array functions
     *
     * @param array $extra
     * @param array $field
     * @static
     * @access public
     * @return void
     */
    public static function generateExtra($field, array $extra = array() ){
        $attrs = null;

        if($field && sizeof($field) > 0) {
            if( isset($field['editable']) && !$field['editable'] ) {
                $attrs .= " readonly";
            }
        }

        if(sizeof($extra) > 0) {
            foreach ($extra as $key => $i ) $attrs .= ' '.$key.'="'.$i.'"';
        }

        return $attrs;
    }
    /* }}} */

    /* public getRelation(array $field, array $data) {{{ */
    /**
     * getRelation
     *
     * @description return what an object must show on form or an a list if its a relation. In case of a field is a select, it return as object array.
     *
     * @param array $field
     * @param array $data
     * @static
     * @access public
     * @return void
     */
    public static function getRelation(array $field, array $data){
        $result = null;

        if (isset($field['relation']) && $field['relation'] ) {
            $model  = Helper::generateModel(isset($field['mutator'])?$field['mutator']:$field['field']);

            if($field['type'] == "text") {
                $value  = $model::find($data['obj'][$field['field']]);
                $result = $value->presentation();
            } else if ($field['type'] == "select") {
                $value  = $model::orderBy('created_at','DESC')->take(1000)->get();

                foreach($value as $item){
                    $result[] = $item;
                }

            }

        }

        return $result;
    }
    /* }}} */


}
