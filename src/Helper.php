<?php
namespace LocknLoad\Crud;

use LocknLoad\Crud\Fields;
use View;
use DB;

class Helper
{

    public static function translateField($name){
        $name = explode("_",$name);
        return $name[sizeof($name) -1];
    }

    public static function generateView($type, $data) {
        if(View::exists($type)) return View::make($type)->with('data',$data);
        else return View::make('admin::.'.$type)->with('data',$data);
    }

    public static function generateJson($data){
        return response()->json($data);
    }

    public static function generateModel($class) {
        $class = preg_replace("/id_/","",$class);
        return env("APP_NAMESPACE").'\\'.studly_case($class);
    }

    public static function isColumnValid($column) {
        $haystack = array("senha", "deleted_at","updated_at", "remember_token");

        foreach($haystack as $item) {
            if ( preg_match("/$item/i",$column,$m) ) {
                return false;
            }
        }

        return true;
    }

    public static function isFieldsInList($model, $col, $filterFields = false) {

        $haystack = $model->getFieldsInList();


	if (!empty($haystack) && $filterFields) {
		return in_array($col, $haystack); 
        } else {
            return true;
	}


    }

    public static function getColumnInfo($model, $column){

        $columns  = $model->getColumns();

        foreach ($columns as $col) {

            if ( $col->Field == $column) {
                return Fields::setFieldInfo($col);
                break;
            }

        }

        return null;

    }

    public static function getModelColumns($model, $filterFields = false)
    {
        $result   = array();
        $columns  = $model->getColumns();

        foreach ($columns as $col) {

            if ( self::isColumnValid($col->Field) && self::isFieldsInList($model, $col->Field, $filterFields)) {
                $result[] = Fields::setFieldInfo($col);
            }

        }

        return $result;
    }

    public static function getManyToManyOptions($obj){

        $result = array();
        $model  = self::generateModel($obj['field']);
        $items  = $model::get();

        foreach($items as $item){
            $result['root'][$item->id] = array( 'item' => $item->presentation(), 'subitem' => null );

            if(method_exists($item, 'getSubItems')) {
                foreach ( $item->getSubItems as $subitem ) {
                    $result['root'][$item->id]['subitem'] = array($subitem->id => $subitem->presentation() );
                }
            }
        }

        return $result;
    }

     public static function getCombination($combination){
      return array();
     }

     public static function itemHasOption($class, $option, $id, $idOption){
        if ($id > 0) {
            $model = self::generateModel($class);
            $ids = $model::find($id)->$option()->pluck('id')->toArray();

            if (sizeof($ids) > 0 && in_array($idOption, $ids)) return true;
            else return false;
        } else {
            return false;
        }
    }

    public static function getColumns($obj){
        return DB::select('SHOW COLUMNS FROM '.$obj);
    }

    public static function getManyToMany($class){

        $model = self::generateModel($class);
        return $model::getManyToMany();

    }

    public static function getFilterItems($class){

        if ($class) {
            $model  = self::generateModel($class);
            $items  = $model::get();

            return $items;
        } else {
            return null;
        }
    }

     public static function compileStruct($snipet, $content){
          $r = $snipet;
	  if(sizeof($content['attributes']) > 0){
		  foreach($content['attributes'] as $key => $c){
             		$par = '/\{\{'.$key.'\}\}/';
	        	$r = preg_replace($par,$c ,$r);
		}
          }
          return $r;
     }

}
